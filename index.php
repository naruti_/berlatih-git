<?php
require_once("animal.php");
require_once("ape.php");
require_once("frog.php");




$sheep = new Animal("shaun");

echo "Name: ".$sheep->name; // "shaun"
echo "<br>";
echo "legs: ".$sheep->legs; // 4
echo "<br>";
echo "cold blooded: ".$sheep->cold_blooded; // "no"
echo "<br>";

$kodok = new Frog("buduk");

echo "<br>";
echo "Name: ".$kodok->name; 
echo "<br>";
echo "legs: ".$kodok->legs; 
echo "<br>";
echo "cold blooded: ".$kodok->cold_blooded; 
echo "<br>";
echo "Jump: ".$kodok->jump(); 
echo "<br>";


$sungokong = new Ape("kera sakti");

echo "<br>";
echo "Name: ".$sungokong->name; 
echo "<br>";
echo "legs: ".$sungokong->legs; 
echo "<br>";
echo "cold blooded: ".$sungokong->cold_blooded; 
echo "<br>";
echo "Yell: ".$sungokong->yell(); 
